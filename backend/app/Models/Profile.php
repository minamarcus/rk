<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use DB;

class Profile extends Model {
    protected $table = "profiles";

    public function list(Request $request)
    {
        return Profile::with('user:id,name,email')->get();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }


}