<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use PhpParser\Error;

class UsersController  extends Controller {

    protected $userModel;

    public function __construct(Request $request) {
        $this->userModel = new User;
    }

    /**
     * Lists all users
     *
     * @return User[]
     */
    public function list() {
        $data = User::all();
        return response()->json($data);
    }

    /**
     * @param $id
     *
     * @return User
     */
    public function detail($id) {
        return response()->json(User::find($id));
    }


    /**
     * @param Request $request
     *
     * @return User|Error[]
     */
    public function create(Request $request) {

        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users',
            'password' => 'required|min:8'
        ]);

        if ($validator->fails()) {
            return $validator->messages();
        } else {
            return User::create([
                'name' => $request['name'],
                'email' => $request['email'],
                'password' => bcrypt($request['password'])
            ]);
        }

    }


    /**
     * @param Request $request
     * @param $id
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function update(Request $request, $id) {
        $user = User::find($id);
        if ($user) {
            $validator = Validator::make($request->all(), [
                'email' => 'email|unique:users',
                'password' => 'min:8'
            ]);
            if ($validator->fails()) {
                return $validator->messages();
            } else {
                $user->update($request->has('password') ?
                    array_merge($request->except('password'), ['password' => bcrypt($request->input('password'))])
                    : $request->except('password'));
                return response('User updated', 200);
            }

        } else {
            return response('User not found', 404);
        }


    }


}