<?php

namespace App\Http\Controllers;
use App\Models\Profile;
use Illuminate\Http\Request;

class ProfilesController  extends Controller {

    protected $profileModel;

    public function __construct(Request $request) {
        $this->profileModel = new Profile;
    }

    public function list(Request $req) {
        $data = $this->profileModel->list($req);
        return response()->json($data);
    }
}