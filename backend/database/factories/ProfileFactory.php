<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Profile::class, function (Faker $faker) {
    return [
        'age' => $faker->numberBetween($min = 50, $max = 100),
        'image' => $faker->imageUrl($width = 640, $height = 480),
        'title' => $faker->jobTitle,
        'link' => $faker->url,
        'link_type' => 'linkedin',
        'user_id' => function () {
            return factory(App\Models\User::class)->create()->id;
        },
    ];
});
