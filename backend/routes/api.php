<?php

use Illuminate\Http\Request;

//profiles
Route::get('profile/list',         'ProfilesController@list');
Route::get('profile/detail/{id}',  'ProfilesController@detail');

//users
Route::get('user/list',         'UsersController@list');
Route::get('user/list/{id}',  'UsersController@detail');
Route::post('user/create',  'UsersController@create');
Route::put('user/update/{id}',  'UsersController@update');