Backend

- Please follow instructions on https://laravel.com/docs/5.6/homestead to install Homestead on your local machine.

- Type "cd backend" and type "composer install"

- Set the local url to: api.rk.test

Frontend

- Make sure you have node & npm installed

- Type "cd frontend" and type "npm install"

- Type "npm start"