import React, { Component } from 'react';
import './HomePage.css';

function UserDetails(props) {
    return (
        <div className="User-detail" >
            {props.item.user.name}<br/>
            {props.item.title}<br/>
            <u>{props.item.user.email}</u><br/>
        </div>
    );
}

class User extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            brightness: '100%',
            hover: false
        }
    }
    OnMouse(){
        this.setState({
            brightness: '35%',
            hover: true
        });
    }
    OutMouse(){
        this.setState({
            brightness: '100%',
            hover: false
        });
    }
    render() {
        let details = '';
        if (this.state.hover) {
            details = <UserDetails item={this.props.item}/>;
        } else {
            details = '';
        }
        return (
            <div
                className="User-box"
                onMouseEnter={() => this.OnMouse()}
                onMouseLeave={() => this.OutMouse()}
            >
                {details}
                <div
                    className="Profile-picture"
                    style={{backgroundImage: `url(${this.props.item.image})`,
                        filter: `brightness(${this.state.brightness})`}}
                >

                </div>
            </div>
        );

    }
}

class HomePage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: []
        };
    }

    componentDidMount() {
        fetch("http://api.rk.test/api/profile/list")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        items: result
                    });
                },
                (error) => {
                    console.log('error: ', error);
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    render() {
        const { error, isLoaded, items } = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return (
                <div className="HomePage">
                    <div className="Users">
                        <div>Loading...</div>
                    </div>
                </div>
            );
        } else {
            return (
                <div className="HomePage">
                    <div className="Users">
                        {items.map(item => (
                            <User
                                item={item}
                                key={item.id}
                            />
                        ))}
                    </div>
                </div>
            );
        }
    }
}
export default HomePage;