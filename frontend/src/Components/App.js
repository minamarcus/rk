import React, { Component } from 'react';
import headerImage from './header-pic.png';
import logo from './logo.png';
import './App.css';
import HomePage from './HomePage';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
            <img src={logo} className="logo" alt="logo" />
        </header>
          <div className="Header-pic" style={{backgroundImage: `url(${headerImage})`}}>
              <div className="header-box">
                  <div className="header-message">
                      <p>
                          Welcome to Smoke & Mirrors. Your one location for advice.
                      </p>
                  </div>

                  <div className="find-button">
                          Find out more
                  </div>
              </div>
          </div>
          <HomePage />
      </div>
    );
  }
}

export default App;